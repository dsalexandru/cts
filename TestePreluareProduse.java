package teste;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestePreluareProduseCautaDenumire.class,
		TestePreluareProduseExecute.class,
		TestePreluareProduseCautaPret.class })
public class TestePreluareProduse {

}
