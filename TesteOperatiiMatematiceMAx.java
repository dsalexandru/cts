package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

import clase.GenerareExempleComenzi;
import clase.IstoricComenzi;
import clase.OperatiiMatematice;
import clase.Produs;

public class TesteOperatiiMatematiceMAx {

	private ArrayList<IstoricComenzi> comenzi ;
	private OperatiiMatematice operatii;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		GenerareExempleComenzi generareComenzi = mock(GenerareExempleComenzi.class);
		when(generareComenzi.GenerareExemenpleComenziLunare()).then(new Answer<ArrayList<IstoricComenzi>>() {

			@Override
			public ArrayList<IstoricComenzi> answer(InvocationOnMock invocation) throws Throwable {
				ArrayList<IstoricComenzi> comenziL = new ArrayList<IstoricComenzi>();
				IstoricComenzi comanda = new IstoricComenzi(new ArrayList<Produs>());
				comanda.setValoare(32.08);
				comanda.setData("12/4/2015");
				comenziL.add(comanda);
				comanda = new IstoricComenzi(new ArrayList<Produs>());
				comanda.setValoare(78.84);
				comanda.setData("12/3/2015");
				comenziL.add(comanda);
				comanda = new IstoricComenzi(new ArrayList<Produs>());
				comanda.setValoare(34.95);
				comanda.setData("12/2/2015");
				comenziL.add(comanda);
				return comenziL;
			}
		});
		comenzi = generareComenzi.GenerareExemenpleComenziLunare();
		operatii = new OperatiiMatematice(comenzi);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMaxComenzi() {
		assertEquals(78.84, operatii.getMax(),0.05);
	}
	
	@Test
	public void testMaxComenziListaGoala() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(Double.MAX_VALUE, operatii2.getMax(),0.05);
	}
	
	@Test
	public void testMaxComenziListaCuElemNull() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		comenzi2.add(null);
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(Double.MAX_VALUE, operatii2.getMax(),0.05);
	}
	
	@Test
	public void testMaxComenziListaCuElemNegative() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		IstoricComenzi comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(-21.22);
		comenzi2.add(comanda);
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(-21.22, operatii2.getMax(),0.05);
	}
	
	@Test
	public void testMaxComenziListaCuElemNegativeNulleSiPozitive() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		IstoricComenzi comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(-21.22);
		comenzi2.add(comanda);
		comenzi2.add(null);
		comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(21.22);
		comenzi2.add(comanda);
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(21.22, operatii2.getMax(),0.05);
	}

}
