package teste;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.PreiaProduseCora;
import clase.Produs;

public class TestePreluareProduseExecute {

	private static BufferedReader reader;
	private static ArrayList<Produs> produsele;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		//System.out.println("setUpBeforeClass()");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//System.out.println("tearDownAfterClass()");
	}

	@Before
	public void setUp() throws Exception {
		produsele = new ArrayList<Produs>();
		Produs p = new Produs("Salrom sare 1kg", "Sare", 1.43);
		p.setMarca("Salrom");
		produsele.add(p);
		
		p = new Produs("Salrom sare 600g", "Sare", 4.56);
		p.setMarca("Salrom");
		produsele.add(p);
		
		p = new Produs("Salrom sare 1kg" , "Sare", 1.46);
		p.setMarca("Salrom");
		produsele.add(p);
		
		p = new Produs("Salrom sare 280g", "Sare", 3.69);
		p.setMarca("Salrom");
		produsele.add(p);
		

		reader = new BufferedReader(new FileReader(new File("CoraHTML.txt")));
	}

	@After
	public void tearDown() throws Exception {
		reader.close();
	}

	@Test
	public void testExecuteSize() {
		PreiaProduseCora preia = new PreiaProduseCora();
		assertEquals(4, preia.execute(reader, "Sare", "Salrom", "Cora").size());
		
	}

	@Test
	public void testExecuteListNotNull() {
		PreiaProduseCora preia = new PreiaProduseCora();
		ArrayList<Produs> produse = new ArrayList<Produs>();
		produse.addAll(preia.execute(reader, "Sare", "Salrom",
				"Cora"));
		assertNotNull(produse);
	}

	@Test
	public void testExecuteObjectsNotNull() {
		PreiaProduseCora preia = new PreiaProduseCora();
		ArrayList<Produs> produse = preia.execute(reader, "Sare", "Salrom",
				"Cora");
		for (Produs produs : produse) {
			assertNotNull(produs);
		}
	}
	
	@Test
	public void testExecuteObjects() {
		PreiaProduseCora preia = new PreiaProduseCora();
		ArrayList<Produs> lista = preia.execute(reader, "Sare", "Salrom","Cora");
		for(int i=0;i<produsele.size();i++){
			assertEquals(produsele.get(i), lista.get(i));
		}
		
	}

}
