package teste;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TesteDesenareGraficValoriLunare.class,
		TesteDesenareGraficValoriSaptamanale.class })
public class TesteDesenareGraficGetValori {

}
