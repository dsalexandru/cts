package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.PreiaProduseCora;

public class TestePreluareProduseCautaPret {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCautaPret() {
		PreiaProduseCora preia = new PreiaProduseCora();
		Double pret = preia.cautaPret("alt=\"Salrom sare 1kg 1,43 lei\"");
		assertEquals(1.43, pret, 0.01);
	}
	
	@Test
	public void testCautaPretAlt() {
		PreiaProduseCora preia = new PreiaProduseCora();
		Double pret = preia.cautaPret("alt=\"\"");
		assertEquals(0, pret, 0.01);
	}
	
	@Test
	public void testCautaPretNull() {
		PreiaProduseCora preia = new PreiaProduseCora();
		Double pret = preia.cautaPret(null);
		assertEquals(0, pret, 0.01);
	}

}
