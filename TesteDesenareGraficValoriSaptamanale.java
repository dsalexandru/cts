package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;
import clase.DesenareGrafic;
import clase.GenerareExempleComenzi;
import clase.IstoricComenzi;
import clase.Produs;

public class TesteDesenareGraficValoriSaptamanale {

	private ArrayList<IstoricComenzi> comenzi;
	private ArrayList<Double> valorileSaptamanale;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ArrayList<IstoricComenzi> lista = new ArrayList<IstoricComenzi>();
		IstoricComenzi comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(32.08);
		comanda.setData("1/"+((Integer)(Calendar.getInstance().get(Calendar.MONTH) + 1)).toString()+"/2015");
		lista.add(comanda);
		comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(53.84);
		comanda.setData("6/"+((Integer)(Calendar.getInstance().get(Calendar.MONTH) + 1)).toString()+"/2015");
		lista.add(comanda);
		comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(34.95);
		comanda.setData("2/"+((Integer)(Calendar.getInstance().get(Calendar.MONTH) + 1)).toString()+"/2015");
		lista.add(comanda);
		comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(70.93);
		comanda.setData("23/"+((Integer)(Calendar.getInstance().get(Calendar.MONTH) + 1)).toString()+"/2015");
		lista.add(comanda);
		GenerareExempleComenzi generareComenzi = mock(GenerareExempleComenzi.class);
		when(generareComenzi.GenerareExemenpleComenziSaptamanale()).thenReturn(lista);
		
		comenzi = generareComenzi.GenerareExemenpleComenziSaptamanale();
		valorileSaptamanale = new ArrayList<Double>();
		valorileSaptamanale.add(120.87);
		valorileSaptamanale.add(0.00);
		valorileSaptamanale.add(0.00);
		valorileSaptamanale.add(70.93);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetValoriSaptamanaleNotNull() {
		DesenareGrafic grafic = new DesenareGrafic(comenzi);
		ArrayList<Double> valoriSaptamanale = grafic.getValoriSaptamanale();
		assertNotNull(valoriSaptamanale);
	}

	@Test
	public void testGetValoriLunareObjectsNotNull() {
		DesenareGrafic grafic = new DesenareGrafic(comenzi);
		ArrayList<Double> valoriSaptamanale = grafic.getValoriSaptamanale();
		for (Double valoare :valoriSaptamanale) {
			assertNotNull(valoare);
		}
	}

	@Test
	public void testGetValoriLunareObjects() {
		DesenareGrafic grafic = new DesenareGrafic(comenzi);
		ArrayList<Double> valoriSaptamanale = grafic.getValoriSaptamanale();
		for (int i = 0; i <valoriSaptamanale.size(); i++) {
			assertEquals(valorileSaptamanale.get(i),valoriSaptamanale.get(i), 0.001);
			
		}

	}
	
	@Test
	public void testGetValoriSaptamanaleNull() {
		DesenareGrafic grafic = new DesenareGrafic(null);
		ArrayList<Double> valoriSaptamanale = grafic.getValoriSaptamanale();
		assertNotNull(valoriSaptamanale);
	}
	
	@Test
	public void testGetValoriSaptamanaleEmptyList() {
		DesenareGrafic grafic = new DesenareGrafic(new ArrayList<IstoricComenzi>());
		ArrayList<Double> valoriSaptamanale = grafic.getValoriSaptamanale();
		for(int i=0;i<4;i++){
			assertEquals(0.00, valoriSaptamanale.get(i),0.0000001);
		}
	}
	
	@Test
	public void testGetValoriSaptamanaleListWithNullElement() {
		ArrayList<IstoricComenzi> lista = new ArrayList<IstoricComenzi>();
		lista.add(null);
		DesenareGrafic grafic = new DesenareGrafic(lista);
		ArrayList<Double> valoriSaptamanale = grafic.getValoriSaptamanale();
		for(int i=0;i<4;i++){
			assertEquals(0.00, valoriSaptamanale.get(i),0.0000001);
		}
	}

	


}
