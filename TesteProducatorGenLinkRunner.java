package teste;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TesteProducatorGenLinkRunner {

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(TesteProducatorGenLink.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println("Testele s-au efectuat fara erori si fara failuri!");
	}

}
