package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import strategy.Producator;
import strategy.ProducatorSareCora;


public class TesteProducatorCoraGenLink {

	private Producator producator;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		producator = new ProducatorSareCora("Salrom");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenLink() {
		assertEquals("http://www.coradrive.ro/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&maxPrice=&showResultsPage=true&langId=-21&beginIndex=0&sType=SimpleSearch&metaData=&pageSize=&manufacturer=&resultCatEntryType=&catalogId=10551&pageView=image&searchTerm=&facet=mfName_ntk_cs%253A%2522SALROM%2522&minPrice=&categoryId=20663&storeId=10801", producator.generateLink());
	}
	
	@Test
	public void testGenLinkCuUnSpatiu() {
		producator = new ProducatorSareCora("Camara Anitei");
		assertEquals("http://www.coradrive.ro/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&maxPrice=&showResultsPage=true&langId=-21&beginIndex=0&sType=SimpleSearch&metaData=&pageSize=&manufacturer=&resultCatEntryType=&catalogId=10551&pageView=image&searchTerm=&facet=mfName_ntk_cs%253A%2522CAMARA%2BANITEI%2522&minPrice=&categoryId=20663&storeId=10801", producator.generateLink());
	}
	
	@Test
	public void testGenLinkCuMaiMulteSpatii() {
		producator = new ProducatorSareCora("Pivnita cu bunataturi");
		assertEquals("http://www.coradrive.ro/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&maxPrice=&showResultsPage=true&langId=-21&beginIndex=0&sType=SimpleSearch&metaData=&pageSize=&manufacturer=&resultCatEntryType=&catalogId=10551&pageView=image&searchTerm=&facet=mfName_ntk_cs%253A%2522PIVNITA%2BCU%2BBUNATATURI%2522&minPrice=&categoryId=20663&storeId=10801", producator.generateLink());
	}

}
