package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.GenerareExempleComenzi;
import clase.IstoricComenzi;
import clase.OperatiiMatematice;
import clase.Produs;

public class TesteOperatiiMatematiceMin {

	private ArrayList<IstoricComenzi> comenzi ;
	private OperatiiMatematice operatii;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		comenzi = new GenerareExempleComenzi().GenerareExemenpleComenziSaptamanale();
		operatii = new OperatiiMatematice(comenzi);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testMinComenzi() {
		assertEquals(23.33, operatii.getMin(),0.05);
	}
	
	@Test
	public void testMinComenziListaGoala() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(Double.MIN_VALUE, operatii2.getMin(),0.05);
	}
	
	@Test
	public void testMinComenziListaCuElemNull() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		comenzi2.add(null);
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(Double.MIN_VALUE, operatii2.getMin(),0.05);
	}
	
	@Test
	public void testMinComenziListaCuElemNegative() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		IstoricComenzi comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(-21.22);
		comenzi2.add(comanda);
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(-21.22, operatii2.getMin(),0.05);
	}
	
	@Test
	public void testMinComenziListaCuElemNegativeNulleSiPozitive() {
		ArrayList<IstoricComenzi> comenzi2 = new ArrayList<IstoricComenzi>();
		IstoricComenzi comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(-21.22);
		comenzi2.add(comanda);
		comenzi2.add(null);
		comanda = new IstoricComenzi(new ArrayList<Produs>());
		comanda.setValoare(21.22);
		comenzi2.add(comanda);
		OperatiiMatematice operatii2 = new OperatiiMatematice(comenzi2);
		assertEquals(-21.22, operatii2.getMin(),0.05);
	}

}
