package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.PreiaProduseCora;

public class TestePreluareProduseCautaDenumire {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCautaDenumire() {
		PreiaProduseCora preia = new PreiaProduseCora();
		String denumire = preia.cautaDenumire("alt=\"Salrom sare 1kg 1,43 lei\"");
		assertEquals("Salrom sare 1kg", denumire);
	}
	
	@Test
	public void testCautaDenumireAlt() {
		PreiaProduseCora preia = new PreiaProduseCora();
		String denumire = preia.cautaDenumire("alt=\"\"");
		assertEquals(null, denumire);
	}
	
	@Test
	public void testCautaDenumireNotNull() {
		PreiaProduseCora preia = new PreiaProduseCora();
		String denumire = preia.cautaDenumire("alt=\"Salrom sare 1kg 1,43 lei\"");
		assertNotNull(denumire);
	}
	
	@Test
	public void testCautaDenumireNull() {
		PreiaProduseCora preia = new PreiaProduseCora();
		String denumire = preia.cautaDenumire("");
		assertEquals(null, denumire);
	}

}
