package teste;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TesteDesenareGraficGetValori.class,
		TesteOperatiiMatematice.class,
		TestePreluareProduse.class,
		TesteProducatorGenLink.class })
public class AllTests {

}
