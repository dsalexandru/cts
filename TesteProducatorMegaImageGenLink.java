package teste;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import strategy.Producator;
import strategy.ProducatorSareMegaImage;


public class TesteProducatorMegaImageGenLink {

	private Producator producator;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		producator = new ProducatorSareMegaImage("Salrom");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenLink() {
		assertEquals("http://www.emag.ro/supermarket/sare/salrom/c", producator.generateLink());
	}
	
	@Test
	public void testGenLinkCuvinteCuSpatiu() {
		producator = new ProducatorSareMegaImage("Camara cu bunataturi");
		assertEquals("http://www.emag.ro/supermarket/sare/camara-cu-bunataturi/c", producator.generateLink());
	}

	@Test
	public void testGenLinkCuApostrof() {
		producator = new ProducatorSareMegaImage("O'Neil");
		assertEquals("http://www.emag.ro/supermarket/sare/o-neil/c", producator.generateLink());
	}
	
	@Test
	public void testGenLinkCuSpatiuSiApostrof() {
		producator = new ProducatorSareMegaImage("Cava D'oro salt");
		assertEquals("http://www.emag.ro/supermarket/sare/cava-d-oro-salt/c", producator.generateLink());
	}
	
	@Test
	public void testGenLinkCuvinteCuSi() {
		producator = new ProducatorSareMegaImage("Healthy&Good");
		assertEquals("http://www.emag.ro/supermarket/sare/healthy-good/c", producator.generateLink());
	}
}
