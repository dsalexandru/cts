package teste;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import clase.DesenareGrafic;
import clase.GenerareExempleComenzi;
import clase.IstoricComenzi;

public class TesteDesenareGraficValoriLunare {

	private ArrayList<IstoricComenzi> comenzi;
	private ArrayList<Double> valorileLunare;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		GenerareExempleComenzi generareComenzi = new GenerareExempleComenzi();
		comenzi = generareComenzi.GenerareExemenpleComenziLunare();
		valorileLunare = new ArrayList<Double>();
		valorileLunare.add(40.23);
		valorileLunare.add(34.95);
		valorileLunare.add(53.84);
		valorileLunare.add(32.08);
		valorileLunare.add(43.59);
		valorileLunare.add(23.33);
		valorileLunare.add(37.90);
		valorileLunare.add(78.90);
		valorileLunare.add(65.90);
		valorileLunare.add(10.90);
		valorileLunare.add(130.90);
		valorileLunare.add(56.90);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetValoriLunareNotNull() {
		DesenareGrafic grafic = new DesenareGrafic(comenzi);
		ArrayList<Double> valoriLunare = grafic.getValoriLunare();
		assertNotNull(valoriLunare);
	}

	@Test
	public void testGetValoriLunareObjectsNotNull() {
		DesenareGrafic grafic = new DesenareGrafic(comenzi);
		ArrayList<Double> valoriLunare = grafic.getValoriLunare();
		for (Double valoare : valoriLunare) {
			assertNotNull(valoare);
		}
	}

	@Test
	public void testGetValoriLunareObjects() {
		DesenareGrafic grafic = new DesenareGrafic(comenzi);
		ArrayList<Double> valoriLunare = grafic.getValoriLunare();
		for (int i = 0; i < valoriLunare.size(); i++) {
			assertEquals(valorileLunare.get(i), valoriLunare.get(i), 0.001);
		}

	}

	@Test
	public void testGetValoriLunareNull() {
		DesenareGrafic grafic = new DesenareGrafic(null);
		ArrayList<Double> valoriLunare = grafic.getValoriLunare();
		assertNotNull(valoriLunare);
	}

	@Test
	public void testGetValoriLunareEmptyList() {
		DesenareGrafic grafic = new DesenareGrafic(
				new ArrayList<IstoricComenzi>());
		ArrayList<Double> valoriLunare = grafic.getValoriLunare();
		assertNotNull(valoriLunare);
	}

	@Test
	public void testGetValoriLunareEmptyListWithNullElement() {
		ArrayList<IstoricComenzi> lista = new ArrayList<IstoricComenzi>();
		lista.add(null);
		DesenareGrafic grafic = new DesenareGrafic(lista);
		ArrayList<Double> valoriLunare = grafic.getValoriLunare();
		for (int i = 0; i < valoriLunare.size(); i++) {
			assertEquals(0, valoriLunare.get(i),0.001);
		}
	}

}
