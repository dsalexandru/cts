package teste;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TesteOperatiiMatematiceRunner {

	public static void main(String[] args) {
		Result result = JUnitCore.runClasses(TesteOperatiiMatematice.class);
		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}
		System.out.println("Testele s-au efectuat fara erori si fara failuri!");
	}

}
